
## Demo
[Demo] (http://checkplate.herokuapp.com/)

## Run after cloning
```
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/opt \
    -w /opt \
    laravelsail/php80-composer:latest \
    composer install --ignore-platform-reqs
```

## Examples
[Homepage](https://imgur.com/WZsNViz)
[Details](https://imgur.com/pJDEVQv)

## Uitleg keuzes
```
Docker, Laravel, Sass, Jquery, Bootstrap, Webpack - Persoonlijke voorkeur, ben deze setup gewend en vind 'm erg fijn werken.

De belangrijkste componenten.

RDW VehicleRepository - Eerder een service dan een repository, fetched auto gegevens van het RDW aan de hand van het ingegeven kenteken. momenteel word the endpoint uit de config gevist, als ik dit helemaal uit zou bouwen dan zou ik van RDW een aparte VehicleRepository (Service) maken die op zichzelf te injecten is zonder dependencies buiten zichzelf.

ViaBovagAverager - Webcrawler, ze hebben geen publieke API om gebruik van te maken, maar wel een hoop search results. 't idee was om een averager service te maken waar ViaBovag er eentje van is. bouwt een url naar viabovag search results. dit zijn er duizenden dus om laad tijden te voorkomen word er een sample size uit de duurste en goedkoopste exemplaren gepakt en die word geaveraged.

Vehicle - Model/JsonResource, de mapping van de RDW voertuig gegevens. mochten er ooit keys veranderen in de RDW api dan hoeft alleen het model aangepast te worden, de rest van de code refereert alleen daarnaar. 

validation zorgt ervoor dat er helemaal niks gebeurd tenzij je input door deze regex heen komt:

// https://www.npmjs.com/package/rdw-kenteken-check
const arrRegEx = ['^([A-Z]{2})([0-9]{2})([0-9]{2})$', // XX9999
    '^([0-9]{2})([0-9]{2})([A-Z]{2})$', // 9999XX
    '^([0-9]{2})([A-Z]{2})([0-9]{2})$', // 99XX99
    '^([BDFGHJKLMNPRSTVWXYZ]{2})([0-9]{2})([BDFGHJKLMNPRSTVWXYZ]{2})$',// XX99XX
    '^([BDFGHJKLMNPRSTVWXYZ]{2})([BDFGHJKLMNPRSTVWXYZ]{2})([0-9]{2})$',// XXXX99
    '^([0-9]{2})([BDFGHJKLMNPRSTVWXYZ]{2})([BDFGHJKLMNPRSTVWXYZ]{2})$',// 99XXXX
    '^([0-9]{2})([BDFGHJKLMNPRSTVWXYZ]{3})([0-9]{1})$',// 99XXX9
    '^([0-9]{1})([BDFGHJKLMNPRSTVWXYZ]{3})([0-9]{2})$',// 9XXX99
    '^([BDFGHJKLMNPRSTVWXYZ]{2})([0-9]{3})([BDFGHJKLMNPRSTVWXYZ]{1})$',// XX999X
    '^([BDFGHJKLMNPRSTVWXYZ]{1})([0-9]{3})([BDFGHJKLMNPRSTVWXYZ]{2})$',// X999XX
    '^((?!PVV|VVD|SGP)[BDFGHJKLMNPRSTVWXYZ]{3})([0-9]{2})([BDFGHJKLMNPRSTVWXYZ]{1})$',// XXX99X
    '^([0-9]{1})([BDFGHJKLMNPRSTVWXYZ]{2})([0-9]{3})$',//9XX999 13
    '^([0-9]{3})([BDFGHJKLMNPRSTVWXYZ]{2})([0-9]{1})$'//999XX9 14
];

Als ik hier meer tijd in zou stoppen dan zou validation/error handling  en jquery opschonen

Er worden geen page loads gedaan, alleen ajax requests naar /check voor RDW gegevens en /average voor ViaBovag.

Mocht ik iets nader moeten beschrijven of verduidelijken dan doe ik dat graag

Met vriendelijke groet,

Niels van Deursen
nhavandeursen@gmail.com
0639648044

```



