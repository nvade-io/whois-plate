<div class="card" >
  <div class="card-header">
    Hebbes
  </div>
  <div class="card-body">
    <h5 class="card-title">
        {{ $data['vehicle']['data']['merk'] }}&nbsp;{{ $data['vehicle']['data']['handelsbenaming'] }}&nbsp;{{ (new \DateTime($data['vehicle']['data']['datum_eerste_afgifte_nederland']))->format('Y') }}
        ,{{ $data['vehicle']['data']['inrichting'] }}
    </h5>
    <p class="card-text">Voor het berekenen van de gemiddelde waarde klikt u op de knop hier beneden.</p>
    <a href="#carDataCollapse" data-bs-toggle="collapse" class="btn btn-outline-secondary" role="button" aria-expanded="false" aria-controls="carDataCollapse">Toon alles</a>
    <button id="button-bereken" class="btn btn-primary">Bereken</button>
    <a hidden id="value-button" data="{{ $data['average_url'] }}" class="btn btn-outline-success">
        <div id="value-spinner" class="spinner-border" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>
    </a>
  </div>
</div>

<div class="collapse" id="carDataCollapse">
    <div class="card">
        <form class="row g-3">
            @foreach ($data['vehicle']['data'] as $property => $value)
                <div class="col-md-6">
                    <label for="input-{{ $property }}" class="form-label">{{ ucfirst(str_replace("_", " ", $property)) }}</label>
                    <input type="text" class="form-control" id="{{ $property }}-input" aria-describedby="{{ $property }}-help" placeholder="{{ $value }}" readonly>
                </div>
            @endforeach
        </form>
    </div>
</div>


