<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" CONTENT="noindex, nofollow">
    @if (is_file(public_path() . '/img/favicon.ico'))
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/img/favicon.ico') }}">
    @endif

    <link rel="stylesheet" href="{{ mix('css/app.css') }}"/>
</head>

<body>
    <main class="content">
        @yield('content')
    </main>
    <script src="{{ mix('js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>

