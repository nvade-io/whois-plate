import 'jquery';
import { Modal } from 'bootstrap';

window.$ = $;
window.jQuery = $;

const carModal = new Modal(document.getElementById("carModal"), {});
var checkingPrice = false;
var alreadyCalculated = false;


// https://www.npmjs.com/package/rdw-kenteken-check
const arrRegEx = ['^([A-Z]{2})([0-9]{2})([0-9]{2})$', // XX9999
    '^([0-9]{2})([0-9]{2})([A-Z]{2})$', // 9999XX
    '^([0-9]{2})([A-Z]{2})([0-9]{2})$', // 99XX99
    '^([BDFGHJKLMNPRSTVWXYZ]{2})([0-9]{2})([BDFGHJKLMNPRSTVWXYZ]{2})$',// XX99XX
    '^([BDFGHJKLMNPRSTVWXYZ]{2})([BDFGHJKLMNPRSTVWXYZ]{2})([0-9]{2})$',// XXXX99
    '^([0-9]{2})([BDFGHJKLMNPRSTVWXYZ]{2})([BDFGHJKLMNPRSTVWXYZ]{2})$',// 99XXXX
    '^([0-9]{2})([BDFGHJKLMNPRSTVWXYZ]{3})([0-9]{1})$',// 99XXX9
    '^([0-9]{1})([BDFGHJKLMNPRSTVWXYZ]{3})([0-9]{2})$',// 9XXX99
    '^([BDFGHJKLMNPRSTVWXYZ]{2})([0-9]{3})([BDFGHJKLMNPRSTVWXYZ]{1})$',// XX999X
    '^([BDFGHJKLMNPRSTVWXYZ]{1})([0-9]{3})([BDFGHJKLMNPRSTVWXYZ]{2})$',// X999XX
    '^((?!PVV|VVD|SGP)[BDFGHJKLMNPRSTVWXYZ]{3})([0-9]{2})([BDFGHJKLMNPRSTVWXYZ]{1})$',// XXX99X
    '^([0-9]{1})([BDFGHJKLMNPRSTVWXYZ]{2})([0-9]{3})$',//9XX999 13
    '^([0-9]{3})([BDFGHJKLMNPRSTVWXYZ]{2})([0-9]{1})$'//999XX9 14
];

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$('input[name=kenteken]').on('keypress keyup', function (e) {

    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        var kenteken = $("input[name=kenteken]").val();

        var isLicencePlate = arrRegEx.some((regex, i) => {
            const re = new RegExp(regex);
            return re.test(kenteken)
        })

        if (!isLicencePlate)
            return;

        if (!checkingPrice) {
            checkingPrice = true;

            // Make spinner visible.
            $('#spinner').removeClass('d-none');

            $.ajax({
                type: 'POST',
                url: "/check",
                data: { plate: kenteken },
                success: function (data) {
                    $("input[name=kenteken]").val('');

                    $('.modal-body').html(data);

                    carModal.toggle();
                    checkingPrice = false;

                    $('#spinner').addClass('d-none');
                }
            });
        }
    }
});

$(document).on('click', '#button-bereken', function (e) {
    e.preventDefault();
    console.log('test');
    alreadyCalculated = true;
    document.getElementById('value-button').removeAttribute('hidden');

    var data = $('#value-button').attr('data');
    console.log(data);


    $.ajax({
        type: 'POST',
        url: "/average",
        data: sessionStorage.getItem('vehicle'),
        success: function (data) {

            $('#value-spinner').add('d-none');
            $('#value-button').html('<p class="m-0">€' + data + '</p>');
        }
    });
});

