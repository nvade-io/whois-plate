<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $fillable = [
        'kenteken',
        'voertuigsoort',
        'merk',
        'handelsbenaming',
        'vervaldatum_apk',
        'datum_tenaamstelling',
        'bruto_bpm',
        'inrichting',
        'aantal_zitplaatsen',
        'eerste_kleur',
        'tweede_kleur',
        'aantal_cilinders',
        'cilinderinhoud',
        'massa_ledig_voertuig',
        'toegestane_maximum_massa_voertuig',
        'massa_rijklaar',
        'maximum_massa_trekken_ongeremd',
        'maximum_trekken_massa_geremd',
        'zuinigheidslabel',
        'datum_eerste_toelating',
        'datum_eerste_afgifte_nederland',
        'wacht_op_keuren',
        'wam_verzekerd',
        'aantal_deuren',
        'aantal_wielen',
        'afstand_hart_koppeling_tot_achterzijde_voertuig',
        'afstand_voorzijde_voertuig_tot_hart_koppeling',
        'lengte',
        'breedte',
        'europese_voertuigcategorie',
        'plaats_chassisnummer',
        'technische_max_massa_voertuig',
        'typegoedkeuringsnummer',
        'variant',
        'uitvoering',
        'volgnummer_wijziging_eu_typegoedkeuring',
        'vermogen_massarijklaar',
        'wielbasis',
        'export_indicator',
        'openstaande_terugroepactie_indicator',
        'taxi_indicator',
        'maximum_massa_samenstelling',
        'aantal_rolstoelplaatsen',

    ];

    protected $hidden = [
        'api_gekentekende_voertuigen_assen',
        'api_gekentekende_voertuigen_brandstof',
        'api_gekentekende_voertuigen_carrosserie',
        'api_gekentekende_voertuigen_carrosserie_specifiek',
        'api_gekentekende_voertuigen_voertuigklasse'
    ];
}
