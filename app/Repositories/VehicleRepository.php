<?php

namespace App\Repositories;


use App\Models\Vehicle;

class VehicleRepository extends BaseRepository implements VehicleRepositoryInterface
{
    public function __construct()
    {
        parent::__construct(config('app.rdw.endpoints.vehicle'));
    }

    public function find($plate): ?Vehicle
    {
        $response = parent::query('kenteken', $plate);

        if (!$response->status() == 200)
            return null;

        $model = \App\Models\Vehicle::hydrate($response->json());

        return $model->flatten()->first();
    }
}
