<?php

namespace App\Repositories;

use App\Models\Vehicle;

interface VehicleRepositoryInterface
{
    public function find($plate): ?Vehicle;
}
