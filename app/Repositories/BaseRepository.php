<?php

namespace App\Repositories;


use Illuminate\Support\Facades\Http;

abstract class BaseRepository
{
    /** API endpoint */
    protected $endpoint;

    public function __construct($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function query($key, $value): \Illuminate\Http\Client\Response
    {
        return Http::get($this->endpoint, [$key => $value]);
    }
}
