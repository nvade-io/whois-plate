<?php

namespace App\Repositories;


interface BaseRepositoryInterface
{
    /**
     * Find where $key is $value.
     *
     * @param  string  $key property name.
     * @param string $value property value.
     * @return mixed
     */
    public function query($key, $value): \Illuminate\Http\Client\Response;

}
