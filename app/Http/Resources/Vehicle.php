<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class Vehicle extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'kenteken' => $this->kenteken,
                'voertuigsoort' => $this->voertuigsoort,
                'merk' => $this->merk,
                'handelsbenaming' => $this->handelsbenaming,
                'vervaldatum_apk' => $this->formatDate($this->vervaldatum_apk),
                'datum_tenaamstelling' => $this->formatDate($this->datum_tenaamstelling),
                'bruto_bpm' => $this->bruto_bpm,
                'inrichting' => $this->inrichting,
                'aantal_zitplaatsen' => $this->aantal_zitplaatsen,
                'eerste_kleur' => $this->eerste_kleur,
                'tweede_kleur' => $this->tweede_kleur,
                'aantal_cilinders' => $this->aantal_cilinders,
                'cilinderinhoud' => $this->cilinderinhoud,
                'massa_ledig_voertuig' => $this->massa_ledig_voertuig,
                'toegestane_maximum_massa_voertuig' => $this->toegestane_maximum_massa_voertuig,
                'massa_rijklaar' => $this->massa_rijklaar,
                'maximum_massa_trekken_ongeremd' => $this->maximum_massa_trekken_ongeremd,
                'maximum_trekken_massa_geremd' => $this->maximum_trekken_massa_geremd,
                'zuinigheidslabel' => $this->zuinigheidslabel,
                'datum_eerste_toelating' => $this->formatDate($this->datum_eerste_toelating),
                'datum_eerste_afgifte_nederland' => $this->formatDate($this->datum_eerste_afgifte_nederland),
                'wacht_op_keuren' => $this->wacht_op_keuren,
                'wam_verzekerd' => $this->wam_verzekerd,
                'aantal_deuren' => $this->aantal_deuren,
                'aantal_wielen' => $this->aantal_wielen,
                'afstand_hart_koppeling_tot_achterzijde_voertuig' => $this->afstand_hart_koppeling_tot_achterzijde_voertuig,
                'afstand_voorzijde_voertuig_tot_hart_koppeling' => $this->afstand_voorzijde_voertuig_tot_hart_koppeling,
                'lengte' => $this->lengte,
                'breedte' => $this->breedte,
                'europese_voertuigcategorie' => $this->europese_voertuigcategorie,
                'plaats_chassisnummer' => $this->plaats_chassisnummer,
                'technische_max_massa_voertuig' => $this->technische_max_massa_voertuig,
                'typegoedkeuringsnummer' => $this->typegoedkeuringsnummer,
                'variant' => $this->variant,
                'uitvoering' => $this->uitvoering,
                'volgnummer_wijziging_eu_typegoedkeuring' => $this->volgnummer_wijziging_eu_typegoedkeuring,
                'vermogen_massarijklaar' => $this->vermogen_massarijklaar,
                'wielbasis' => $this->wielbasis,
                'export_indicator' => $this->export_indicator,
                'openstaande_terugroepactie_indicator' => $this->openstaande_terugroepactie_indicator,
                'taxi_indicator' => $this->taxi_indicator,
                'maximum_massa_samenstelling' => $this->maximum_massa_samenstelling,
                'aantal_rolstoelplaatsen' => $this->aantal_rolstoelplaatsen
            ],
            'meta' => [

                'api_axis' => $this->api_gekentekende_voertuigen_assen,
                'api_fuel' => $this->api_gekentekende_voertuigen_brandstof,
                'api_bodywork' => $this->api_gekentekende_voertuigen_carrosserie,
                'api_bodywork_specific' => $this->api_gekentekende_voertuigen_carrosserie_specifiek,
                'api_class' => $this->api_gekentekende_voertuigen_voertuigklasse,
            ]
        ];
    }

    private function formatDate($value)
    {

        return (new \DateTime($value))->format('d-m-Y');
    }
}
