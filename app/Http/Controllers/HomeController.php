<?php

namespace App\Http\Controllers;

use App\Repositories\VehicleRepositoryInterface;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Vehicle;
use App\Services\BasePriceAveragerInterface;
use Session;

class HomeController extends Controller
{
    private $vehicleRepository;
    private $priceAverager;
    public function __construct(VehicleRepositoryInterface $vehicleRepository, BasePriceAveragerInterface $priceAverager)
    {
        $this->vehicleRepository = $vehicleRepository;
        $this->priceAverager = $priceAverager;
    }

    public function index()
    {
        return View('home', ['data' => []]);
    }

    public function check(Request $request)
    {
        $plate = $request->input('plate');

        /** @var Vehicle */
        $vehicle = $this->vehicleRepository->find($plate);

        $resource = \App\Http\Resources\Vehicle::make($vehicle);

        Session::put('vehicle', $resource);
        Session::save();

        $data = [
            'vehicle' => $resource->resolve(),
            'average_url' => route('average')
        ];

        return view('forms.plate', ['data' => $data])->render();
    }

    public function average(Request $request)
    {
        if (!$request->session()->has('vehicle'))
            return redirect()->back();

        $vehicle = $request->session()->get('vehicle');

        $average = $this->priceAverager->getPriceAverage($vehicle);

        return $average;
    }
}
