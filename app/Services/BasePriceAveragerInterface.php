<?php

namespace App\Services;

interface BasePriceAveragerInterface
{
    /**
     * Get price average
     */
    public function getPriceAverage($resource);

}
