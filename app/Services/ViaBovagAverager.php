<?php

namespace App\Services;

class ViaBovagAverager implements BasePriceAveragerInterface
{
    private $sample_size;
    private $cars_per_page = 22;

    public function __construct($sample_size = 80)
    {
        $this->sample_size = $sample_size;
    }

    /**
     * @param \App\Http\Resources\Vehicle $resource
     * */
    public function getPriceAverage($resource)
    {
        $resource = $resource->resolve();

        // Initial setup.
        $brand = \Str::lower($resource['data']['merk']);
        $model = \Str::lower(str_replace(' ', '-', $resource['data']['handelsbenaming']));
        $edition = \Str::lower($resource['data']['inrichting']);
        $year = (new \DateTime($resource['data']['datum_eerste_toelating']))->format('Y');
        $sort_modes = ['/sortering-prijsoplopend', '/sortering-prijsaflopend', ''];
        $sort = $sort_modes[0];
        $current_page = 1;
        $url = 'https://www.viabovag.nl/auto/merk-' . $brand . '/trefwoorden-' . $model . '/bouwjaar-vanaf-' . $year . '/' . $edition . '/pagina-';
        libxml_use_internal_errors( 1 );
        $price = 0;
        $car_count = 0;


        // Load first page manually to get the maximum page number, after that we loop.
        $dom  = new \DOMDocument();
        $html = file_get_contents($url . $current_page . $sort);
        $dom->loadHTML( $html );
        $xpath = new \DOMXpath( $dom );

        $max_page_number = $this->maxPageNumber($xpath->query('//@data-pagination-page'));

        // Do we have enough pages to make use of price sorting?
        if ($max_page_number >= 4)
        {
            // Traverse pages/cars until samplesize is reached.
            while ($car_count <= $this->sample_size)
            {
                // Fetch the current page
                $html = file_get_contents($url . $current_page . $sort);

                // fetch car data from the page
                $cars = $xpath->query('//@data-vehicle');
                foreach($cars->getIterator() as $car) {
                    $price += json_decode(trim($car->value), true)["price"];
                    $car_count++;

                    // Are we halfway? then flip sort_mode and back to page 1.
                    if ($car_count >= ($this->sample_size / 2)) {
                        $current_page = 1;
                        $sort = $sort_modes[1];
                    }
                }

                $current_page++;
            }
        }
        else
        {
            while ($current_page <= $max_page_number) {
                // Fetch the current page
                $html = file_get_contents($url . $current_page);

                // fetch car data from the page
                $cars = $xpath->query('//@data-vehicle');
                foreach($cars->getIterator() as $car) {
                    $price += json_decode(trim($car->value), true)["price"];
                    $car_count++;
                }

                $current_page++;
            }

        }





        // sample size / 2 * most expensive prices + sample size / 2 * cheapest prices / car_count
        return round($price / $car_count, 2);
    }

    private function maxPageNumber($page_node)
    {
        $max_page_number = 0;
        foreach($page_node->getIterator() as $node) {
            $max_page_number = ($node->value > $max_page_number) ? $node->value : $max_page_number;
        }

        if ($max_page_number == 0)
            return 1;

        return $max_page_number;
    }
}
