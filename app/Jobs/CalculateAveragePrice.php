<?php

namespace App\Jobs;

use App\Http\Resources\Vehicle;
use App\Repositories\VehicleRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CalculateAveragePrice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $vehicleRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(VehicleRepositoryInterface $vehicleRepository)
    {
        $this->vehicleRepository = $vehicleRepository;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Vehicle $vehicle)
    {

    }
}
