<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Repositories\VehicleRepository;
use App\Repositories\VehicleRepositoryInterface;
use App\Services\BasePriceAveragerInterface;
use App\Services\ViaBovagAverager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(VehicleRepositoryInterface::class, VehicleRepository::class);
        $this->app->singleton(BasePriceAveragerInterface::class, ViaBovagAverager::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        JsonResource::withoutWrapping();
    }
}
