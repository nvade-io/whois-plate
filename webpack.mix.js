const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */


mix
.autoload({
    jquery: ['$', 'window.jQuery']
})
.js('resources/js/app.js', 'public/js')
.sass('resources/scss/app.scss', 'public/css')
.browserSync({
    proxy: 'localhost',
    port: 8282,
    open: false,
    files: [
        'app/**/*.php',
        'resources/views/**/*.php',
        'resources/scss/**/*.scss',
        'public/js/**/*.js',
        'public/css/**/*.css'
    ],
    watchOptions: {
        usePolling: true,
        interval: 500
    }
})

